---
title: "Installation Instructions"
date: 2017-09-22T14:03:05+01:00
draft: false
menu:
  main:
    parent: Documentation
    identifier: Installation
    weight: 10
---

## Basic requirements

1. For an initial install, you will need administrator rights. From versions > 0.5, administrator rights should not be required for upgrades.
1. Microsoft .NET Framework 2.0 or above

## Installation instructions

1. Double click on the installation file to install.
1. If you are upgrading from an earlier version you will find it helpful to either uninstall the earlier version, or put the new version in a different location, to avoid having to tell the installer whether or not to overwrite existing files.
1. Read the licence and accept it to continue.
1. The default settings should be ok for most users.
  * You may not wish to install the source code directory if you do not intend to review or revise it.
  * You may not wish to install the additional module for applying display LUTs if you are installing on a machine where you will not want to obtain a DICOM GSDF calibration.
1. When you reach the end of the installation, the R component will generate an error regarding an add-in for Excel. This is normal, and you can close the message to finish the installation.

## Errors on first run

If you get the following error, please check to see that you have version 2 of the .NET Framework or later installed on your PC

{{< figure src="/images/docs/IQWorksErrorNoDotNet2.png" >}}
