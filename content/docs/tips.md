---
title: "Miscellaneous tips and tricks"
date: 2017-09-23T23:35:54+01:00
draft: false
menu:
  main:
    parent: Documentation
    identifier: TipsTricks
    weight: 30
---

## Properties dialogue

- To view it, right click on the analysis item
- If you can't see sll the help text in the middle make that section bigger by dragging the divider above the text upwards.

## Multiple image selection

- If you want to run analysis tree on several images, each with independent results, open the images as individual images
- If you want to analyse one thing going through several images, for example slice sensitivity profile for CT, then load all images into a single multi-layer image

![Mulitple image selection dialogue](/images/docs/IndividualOrMultiLayer.png)

{{< note title="Note" >}}
When loading several or large images, especially across a network, there can be a delay once you have made this choice. Please wait, the images are being loaded!
{{< /note >}}

## Tips from a IQworks South West User Group Meeting

- Save a tree before closing the image or the tree does not save.
- Do not use an image that has been flipped or altered. This process leads to interpolation of pixel values and the analysis will not work.
- Use only DICOM images in IQworks- JPEG images will not give correct results.
- When opening an image, once the correct image has been chosen untick the ‘show preview’ box before opening the image. Further images can then also be opened this way without the software crashing.
- When calculating MTF set the ‘sample parameter’ to 0.1. This gives results that are comparable to those calculated from MatLab?.
- To produce a .csv file select csv in the report generator and click ‘store data now’ before generating the report. (some people have noted that the software asks if you require the file to be saved but for other users this is not the case)
- The stack profile analysis function only works with a rectangular ROI.

## STP correction speed

The STP correction (Signal Calibrator on bottom LHS - right click for properties) can be quite slow to run if you have an old computer. Once it has run for one image, save the SigCalTable in a convenient folder and with a meaningful name and any subsequent images from the same unit can be STP corrected quickly by opening the signal calibrator properties, clicking load and selecting the saved signal calibrator table.
