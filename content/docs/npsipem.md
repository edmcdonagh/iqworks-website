---
title: "IPEM 32 part VII NPS Analysis"
date: 2017-09-24T20:56:48+01:00
draft: false
menu:
  main:
    parent: Documentation
    identifier: NPSIPEM32
    weight: 40
---

_Content by David Platten_


I've taken the "NPS.xml" analysis tree that comes with IQWorks 0.6 and modified it so that the analysis better fits with what IPEM report 32 part VII suggests. You can download it using the link at the bottom of this page.

- You must use the signal calibrator to ensure that the image is linearised before you begin.
- First the tree uses the general fixer to set the 0,0 coordinate to the centre of the image.
- Don't worry that the region appears to be in the top-left corner of your image - it will be moved when you "Run processes in group"
- A 125 x 125 mm ROI (sub image in NPS-speak) is then positioned at the centre of the image.
- The "NPS Auto 1" analysis is then run on the 125 x 125 mm ROI.
- The sub image is filled with 128x128 pixel regions that overlap one another by 50%.
- A 2D 2nd order polynominal is fitted and subtracted to reduce the effect of low-frequency non-uniformities (note: I don't know if IQWorks applies this to each 128x128 region, or if it is just applied once to the whole 125 x 125 mm subimage. IPEM 32 part VII suggests that the polynomial be applied to each 128x128 region).
- The NNPS is calculated along the x and y axes, using the mean of the whole 125x125 mm sub image in the calculation.
- The zero frequency axes are both excluded in the calculation of the NNPS along each axis.
- The plots are calculated from 7 data lines either side of the axis. The previous version used 3 lines either side.
- You can right-hand click on the graph and copy the data into Excel.

The results of this tree (for the two images I've looked at) agree closely with my own code and also with Nick Marshall's OBJ_IQ_reduced.sav

- [DJP NPS IPEM32 part VII NPS Analysis Tree](/trees/DJP_NPS_analysis_IPEM32_part_VII_revised.xml)
