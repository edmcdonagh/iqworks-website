---
title: "How to..."
date: 2017-09-23T23:27:25+01:00
draft: false
menu:
  main:
    parent: Documentation
    identifier: HowTo
    weight: 20
---

## Open images

This page describes how to open images in IQWorks. Chances are you're just getting started, so we'll keep it simple.

You can open images from the File menu: `File -> open images.`

If you choose `From Files`, then you get a pretty standard Windows dialogue box from which you can browse to where your images are. You can choose multiple selections of files to open by holding down Ctrl or shift while left-clicking on files.

If you choose `From DICOM Directory` then a directory browser will appear. This lists DICOM files with a lot more information e.g. Date, Patient ID, Series number, and so on. Use the Browse button at the top to navigate to a folder, and then click `Update Study List`. This should then display the DICOM files contained in your chosen directory. Depending on your selection in the patient, series and images windows, the number of files that you can open is displayed in the bottom left: click `Load Data` to open these images. You are then given the option to load individual images in separate windows, or load them all as one multi-layer image. The latter is useful if you want to perform the same operation on all images simultaneously.

The shortcuts to these open images options (by file or by DICOM browser) are the first two icons on the menu bar.

With your image(s) opened, they will be displayed in the main window pane of IQWorks. Thumbnails with image names are displayed in the bottom pane. You can select an image by clicking either on the main image or the thumbnail.

The next step is to create an analysis tree and start some analysis of your images: see the CreateAnalysisTree topic.

## Creating an Analysis Tree

Once you have an image / images open in IQWorks, you can create an analysis tree. With an image displayed, you should see on the left hand side window pane the start of your tree. It will say "Process Group ××" where ×× is a number. This is an empty process group. To add your first analysis to the group, select the process group by clicking on it once, and then click on the `Analysis` menu, and choose `Add Analysis -> ROI -> Rectangular ROI`.

You will see that underneath the Process Group heading, an analysis called `RectROI 1` has been added, and a rectangular ROI has appeared on your image. It is red to show that this is the region of interest that is currently selected in your analysis tree. You can drag it around to reposition it on the image, and click and drag the corners to resize it.

Right-click on the `RectROI 1` and click on `properties` to see the ROI properties such as name, geometry, pre-processing options and so on. This is how you see the properties of any analysis process in IQWorks. Some processes (like a simple ROI) have few options, whereas others have quite a few parameters.

You will notice that the `Results` pane at the bottom of the ROI Properties window displays "not processed". Click on `run process` button at the bottom to get your ROI measurements such as mean pixel value and standard deviation. You can also run a process by right-clicking on it in the analysis tree pane and choosing `Run Process`.

Try adding another process group to your tree by right-clicking on the tree and choosing `Add Group`. Populate these groups with different analysis objects. You can easily rename these objects by right-clicking on them and choosing `Rename`, and you can move these objects within the tree by dragging and dropping them into different groups. You should quickly see that you can organise analysis groups together in different branches of your tree.

To select a different ROI process on your image, you must select the ROI process in the tree. You can't just click on the ROI displayed on the image.
