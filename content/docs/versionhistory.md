---
title: "Release notes"
date: 2017-10-01T21:28:01+01:00
draft: false
menu:
  main:
    parent: Documentation
    identifier: VersionHistory
    weight: 50
---

## Version 0.7 – Released at UKRC Meeting, Manchester, UK. 25th June 2012.

- Project migrated to Visual Studio 2010.
  - Because the ClearCanvas DICOM libraries have hooks into version 3.0 of the .NET framework it was necessary to add true entries to the .csproj files for IQWorks and IQAutoLUT. End user presentation is unaffected.
  - To open with Visual Studio 2008, need to open the .sln file and change version number from 11.00 to 10.00 in the first line of the file. The solution will then load successfully. Any warnings which are flagged can be safely ignored.
- Resolved issue with CVIPTools on 64 bit platforms. Build target set explicitly to x86 rather than "Any CPU".
- Ability to clear analysis tree from current image.
- Fixed bug with non-rectangular ROIs where min or max value could be the top left pixel of the bounding rectangle, even if this was not within the ROI.
- Rotation of image by an arbitrary angle to resolve alignment issues.
- File dialog boxes remember previous directories browsed to.
- Root directory for analysis trees can be set globally. e.g. to a network drive.
- Fuji CR AntiLog Signal Calibrator removed.
- New option to ignore pixel scaling factor in MRI images.
- Upgraded to latest version of CVIPTools - 5.3g. Canny Edge detection does not work at present.
- LocalThresholdFraction and LocalThresholdFractionShrink introduced as new edge detection algorithms.

## Version 0.6 – Released at IPEM Quantitative Image Analysis Meeting, 4th October 2010.

- Regions of Interest
  - Introduction of Annular Elliptical ROIs
  - Introduction of Annular Rectangular ROIs
  - Introduction of ability to select a 'Processed Point' for all ROI types, which can be the centre of the ROI or the max or min point. This can be passed to other analysis modules.
  - Improvements to ROI drawing code, to ensure ROIs cannot be resized to be less than zero width or height
- Additional AppInfo stored preference for multiplier which determines default size of inner region of annular ROIs
- Charts can now be included in reports, if this has been specifically provided for as ZOutputs in the code for individual 'IProcess' processing modules
- Noise images
  - Fixed bug in dimensions of new images
  - Added functionality to generate a specified number of layers
- Image Information dialogue now displays and updates pixel size for multi-layered images correctly.
- GeneralFixer now updates pixels size for multi-layered images correctly.
- Event logging / Application profiling
  - Default location of log file now c:\IQWorks\log.txt
  - Event logging and location of log file now fully customisable through Preferences dialog box. Preferences are preserved between sessions through storing to configuration file when the application is closed.
  - Time between log entries is now expressed as the total number of whole and fractional milliseconds.
- NPS
  - Multi-ROI: Visual labelling of ROIs when selected
  - Auto-ROI: Automatic placement of ROIs within a larger "base" ROI is now possible, encompassing full functionality of underlying NPS calculation algorithm
- Underlying algorithm
  - Introduced logging functionality to enable identification of bottlenecks and improve algorithm optimisation
  - Introduced the ability to take into account regional signal variations (e.g. due to heel effect) by normalising ROI signal values by the mean value on a reference ROI, or the global mean
  - Extended NNPS calculation routine to enable normalisation by the mean of a specified ROI, or the global mean.
  - Introduced ability to exclude specified ROIs from the calculation (e.g. due to detector artefacts)
  - Considerable speed improvements through R interface optimisation
  -Restructured linked classes to reduce code duplication and ensure identical algorithm is being applied regardless of ROI selection / placement method
- Progress bars introduced when loading images, processing analysis trees on multiple images, generating reports and storing data.
- Check performed when ReporterGeneral objects are loaded in an analysis tree that all DataExport results they refer to are actually available in the tree. This had previously prevented some older trees storing data.
- Edge Detector
  - New edge detection type of "ThresholdFraction" added which aims to find the edge thresholded by a specified fraction of the range between the min and max pixel values in the ROI
  - Options added to calculate area encompassed by the detected edge.
- DICOM Browser
  - Bug fixed which caused an exception to be thrown when selecting a new study when multiple series were already selected
  - Added label which indicates the number of data files corresponding to the current selection
  - Bug fixed in caching routine which sometimes prevent updated files from being detected
- SimpleMath processing module added. This enables numerical operations (sum, subtract, multiply, divide, average, std dev, variance, max, min, square, square root, raise to power, absolute) to be performed on the outputs of other processing modules.
- Fixed bug which prevent loading of Varian HNx images.
- Updated file-open dialogue box code so that file formats are tested in a more sensible order before IQWorks attempts to display them in the preview pane.
- Ability to generate sum and difference images of multi-layer images, directly from main GUI.
- CNR
  - Able to accommodate more than one ROI for the noise region
  - Calculates noise from difference image when a two layered image is loaded.
- More robust interpolation algorithms have been implemented throughout the code.
- MTF
  - Droege Method: Implemented optional MethodOfThree aliasing correction
- Immediate processing methods
  - Forward FFT
- Graphing of result values
- Command line test mode
- Ability to hide overlaid structures when displaying images
- Statistical comparison module, allowing pixel values within ROIs to be compared to determine whether they are from different distributions. Tests include: Student 's t pooled, Student's t unpooled and Mann Whitney.

## Version 0.3 - First Public Release of Executable and Source Code

- Launched at the UK CT User Group Meeting, Leeds, 21st October 2008.
- Completed removal of dependencies on non-distributable libraries.
- Removed dependency on .NET Framework 3.5. The whole package will now run under .NET Fraemwork 2.0, which is installed automatically from Windows XP SP2+ and is available for Windows 2000+. We had been experimenting with the parallel processing framework introduced with .NET 3.5, but these features will be left for a future release.
- Various user interface changes.
- Tidied up analysis trees. Only trees which definitely work are now included. Catphan trees augmented.
- Migrated DICOM handling code to use ClearCanvas DICOM library. This is a native .NET implementation so requires less overheads, is faster and results in a smaller download. Compressed and multi-frame DICOM images can now be loaded.
- Implemented a global exception catcher. IQWorks now crashes less frequently and less spectacularly.

## Version 0.2 - First Public Release of Executable

- Launched at the Annual Scientific Meeting of the Institute of Physics and Engineering in Medicine, Bath, 4th September 2008.
- Removed many dependencies on proprietary libraries.
- Many changes to user interface.
- Many changes to numerical analysis routines.

## Version 0.1 - Closed Development Release
