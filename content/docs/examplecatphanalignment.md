---
title: "Example analysis: Catphan 500 alignment slice"
date: 2017-09-24T21:29:03+01:00
draft: false
menu:
  main:
    parent: Documentation
    identifier: ExampleCatphanAlignment
    weight: 50
---

Scanner: GE Lightspeed Pro 32
Warning, this page is currently a work in progress, and is not complete!

## Resources

- Test Image: [Catphan 504 alignment slice, GE Lightspeed Pro 32](/phantoms/Catphan500.dcm)
- Analysis Tree: [Catphan_500_Alignment.xml](/trees/Catphan_500_Alignment.xml) - this is the tree that was shipped with version 0.2 (04/09/2008) of IQWorks.

## Procedure

1. Launch IQWorks

1. Open the image file downloaded from the link in Resources above (Catphan500.dcm)  
  ![Catphan Image opened in IQWorks](/images/docs/catphan500alignment/catphan500opened.jpg)

1. You can improve the look of the image by adjusting the window width and window level using the slide markers on the histogram on the right hand side:  
   ![Improved Window Width and Level](/images/docs/catphan500alignment/catphan500wwwl.jpg)

1. Load the analysis tree:
  - File -> Open Analysis Tree or
  - Click on the third icon from the left: "Load Analysis Tree"
  - Choose either CT Scanner -> Catphan -> Catphan 500 Alignment.xml or
  - Download the analysis tree above and open that version.

1. You should now see the analysis tree on the left, and a series of regions of interest drawn on the image:
   ![Analysis Tree Opened](/images/docs/catphan500alignment/catphan500treeopen.jpg)

1. Right click on the first line of the analysis tree (the tree name): "Catphan 500 Alignment Slice"

1. Select "Run Tree on Current Image"  
   ![Tree Analysed](/images/docs/catphan500alignment/catphan500analysed.jpg)

1. You will see that as the processes in the tree run, some of the regions of interest will show signs of activity, with yellow edge detection lines for example.

## Results

[Report_20080918175950.pdf](/images/docs/catphan500alignment/Report_20080918175950.pdf): CATPHAN 500 Alignment Slice Analysis Report
