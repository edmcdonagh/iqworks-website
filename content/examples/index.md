---
title: "Phantom image library"
date: 2017-10-01T21:50:35+01:00
draft: false
menu:
  main:
    parent: Examples
    identifier: PhantomImages
    weight: 10
---

We are planning to build up a library of images of phantoms and test images from different modalities, and from different manufacturers. This will help us to improve IQWorks, and also demonstrate what it can do.

## CT

| Phantom/Test image | Relevant parameters | Modality details | Image | Discussion/Analysis results |
|---|---|---|---|---|
| CATPHAN 504 Alignment | Slice	120 kV, 400 mA, 0.8 s rotation, 2.5 mm slice | GE Lightspeed Pro 32 | [Catphan500.dcm](/phantoms/Catphan500.dcm) | [Example analysis: Catphan 500 alignment slice](/docs/examplecatphanalignment/) |

## DR

| Phantom/Test image | Relevant parameters | Modality details | Image | Discussion/Analysis results |
|---|---|---|---|---|
| Uniformity image for NNPS	| 75 kV, 1.5 mm Cu, 1.5 m FDD	| Siemens Ysio wireless detector | [ Uniform8uGyImage.zip](/phantoms/Uniform8uGyImage.zip) | [Uniform8uGyImageNNPSresults.zip](/phantoms/Uniform8uGyImageNNPSresults.zip) |
| Image of tungsten edge for MTF analysis	| 75 kV, 1.5 mm Cu, 1.5 m FDD	| Siemens Ysio wireless detector |	[Edge8uGy.zip](/phantoms/Edge8uGy.zip) | [Edge8uGyMTFresults.zip](/phantoms/Edge8uGyMTFresults.zip) |
