---
title: "User generated analysis tree library"
date: 2017-10-01T22:13:01+01:00
draft: false
menu:
  main:
    parent: Examples
    identifier: AnalysisTrees
    weight: 20
---

Help out your colleagues by donating the analysis trees that you have created
for any phantoms that other people might want to use!

Simply attach the analysis tree to this topic, then edit the topic to tell us
what phantom or test the analysis tree is for, and any information that might be
needed to use it.

Analysis trees donated here might in future be distributed with the core IQWorks download.

Thanks!

## Edge_MTF_with_fixer

- [edge_MTF_with_fixer.xml](/trees/edge_MTF_with_fixer.xml): MTF Analysis tree as presented at 2010 IPEM Quantitative Analysis meeting

As presented at the IPEM Quantitative Analysis meeting on 04/10/2010, the analysis tree "edge_MTF_with_fixer" plots an MTF for an edge, and is optimised to match the MTF analysis provided in Nick Marshall's Obj_IQ_reduced. It was created with IQWorks version 0.5.

NB by default, the properties of the general fixer do not change the pixel size. To change this, in the fixer properties adjust "Adjust Pixel Size" to TRUE and enter corrected pixel size.

-- Laurence King - 05 Oct 2010

## Flatness, Deviation and Symmetry (FDS_Analysis)

- [FDS_Analysis.xml](/trees/FDS_Analysis.xml): Flatness, Deviation and Symmetry for 20 x 20 cm field.
- [FDS_User_Notes.pdf](/trees/FDS_User_Notes.pdf): User Notes for Flatness, Deviation and Symmetry Analysis

An analysis tree to calculate flatness, symmetry and deviation of a 20 x 20 cm radiation field. The field does not need to be centred in the image, but must not be at an angle. Flatness and deviation are calculated according to IEC 1989, using a flattened area which is 80% of the width and height of the field size. Symmetry is an approximation to IEC 1989 using sixteen discrete 2 x 2 mm ROIs.

For full details please see the attached PDF (FDS_User_Notes).

This tree is for educational use only and not for clinical applications.

-- Mike Hughes - 24 Jan 2011

## NPS (IPEM 32 part vii method)

David Platten has supplied an analysis tree to calculate NPS, with instructions
under the [Documentation section](/docs/npsipem/) of this site - I thought a
link to it would be useful under this topic.

- [Link to NPS analysis tree](/trees/DJP_NPS_analysis_IPEM32_part_VII_revised.xml)
