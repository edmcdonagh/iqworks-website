---
date: 2016-03-08T21:07:13+01:00
title: IQWorks project
type: index
weight: 0
---

## What is IQWorks

IQWorks is a project to provide Medical Physicists with automated image analysis software for use with DICOM test images, such as CT, mammography and digital radiography.

![IQWorks screenshot](/images/iqworkscatphan.png)

## News

### 27th January 2015: IQWorks v0.7.2 Released

Major changes:

- Can now load and save CVIP images
- Stack profile analysis now uses image number for x-axis if Z slice position is not available
- Bug fixes

## More about IQWorks

- [Where has IQWorks come from?]({{< relref "history.md" >}})
- [Who controls IQWorks now?]({{< relref "ownership.md" >}})
- [IQWorks related references]({{< relref "references.md" >}})
- See the project pages on SourceForge - http://sf.net/p/iqworks
- Download the latest version from [SourceForge (IQWorks v0.7.2)](http://sourceforge.net/projects/iqworks/files/Released%20Versions/IQWorks072/IQWorksSetup_072.exe/download)

## Support

Please use the bug tracker on the SourceForge IQWorks project pages: http://sourceforge.net/p/iqworks/support/

## Analysis trees

Extra community supplied analysis trees can be found in the Downloads section of the wiki
