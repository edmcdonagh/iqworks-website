---
title: "How to get involved in the IQWorks project"
date: 2017-09-22T16:15:20+01:00
draft: false
menu:
  main:
    parent: Getinvolved
    identifier: GetInvolved
    weight: 10
---

There are many ways to help out with the project. The text below is based on a
similar text on the [GNUCash website](http://wiki.gnucash.org/wiki/Development),
and therefore the content is available under
[GNU Free Documentation License 1.2](http://www.gnu.org/copyleft/fdl.html)

## Testing

Programmers can be fine testers, but non-programmers seem to be able to break programs in new and mysterious ways. The trick here is to learn how to give the best information to the programmers about how to reproduce bugs. A programmer will usually only be able to fix a bug they can see; if you can't make the programmer see your bug, it won't get fixed!

- [Create a bug report](https://sourceforge.net/p/iqworks/support/)

## Enhancement suggestions

Providing feedback on what features are used, and what aren't is important to developers who may spend a lot of time on a feature they think is important instead of a feature that actually is important.

- [Make an enhancement suggestion](https://sourceforge.net/p/iqworks/enhancements/)

## Code

If you're a programmer, obviously a good way to help is to start writing useful code :-). Grab the latest git tree and try your hand at one of the outstanding bugs in the
[bug tracker](http://sourceforge.net/p/iqworks/support/) or something on the
[enhancement list](http://sourceforge.net/p/iqworks/enhancements/). We will need:

- Bug fixing
- New analysis algorithms
- QA and verification of existing algorithms
- Improvement of the GUI etc

- Read only clone of git repository: git clone git://git.code.sf.net/p/iqworks/code iqworks-code
- HTTP version if your firewall blocks the git port: git clone http://git.code.sf.net/p/iqworks/code iqworks-code
- Alternatively you can download a zip file of the last released code from http://sf.net/projects/iqworks/files

## Documentation

Documentation is always needed for any project! Even simple things like "Tips and tricks" are a good start. If users can help other users, then the current programmers can spend more of their time getting new developers up to speed. Go over to the Documentation web and get started!

## Test images

To help improve IQWorks we need to get hold of images of phantoms:

- for different modalities (CT, DR, CR, MRI, RT etc)
- and for the same modality but different models and manufacturers (to iron out DICOM compatibility issues)

You can upload your images to PhantomImages

## Analysis trees

If you have a phantom that doesn't have a prebuilt analysis tree, you can help the project by building an analysis tree and donating it to us to distribute with the project.
You can upload your analysis trees to AnalysisTrees

## Advocacy

The best chance that IQWorks has to grow and become the Radiology/Radiotherapy/NM/MR QA Automated Image Analysis tool of choice is if lots of people use it, tell people about it and acknowledge it in publications.
