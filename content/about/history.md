---
title: "History of IQWorks"
date: 2017-09-23T22:14:31+01:00
draft: false
menu:
  main:
    parent: About
    identifier: History
    weight: 10
---

## A PhD project

IQWorks comes from software written by AndrewReilly as part of his PhD research, at the University of Edinburgh. It was written to perform Quality Assurance on digital imaging equipment in the Radiotherapy Department of the Edinburgh Oncology Centre.

## Involvement of the CTUG

The need for Automated Image Analysis was identified at the UK CT User Group meetings. Various options of how to proceed were presented, and the consensus was to pursue Andrew's offer of opening up his software to the wider community under an open source licence.

## Involvement of other UK parties

Other groups such as the [IPEM's](https://www.ipem.ac.uk) Diagnostic Radiology Special Interest Group (DRSIG) were also keen to establish a way that UK Physicists could improve the image analysis performed for QA of Diagnostic Imaging equipment.

A meeting was therefore organised by the IPEM DRSIG to discuss the various approaches that were being taken, and again the concensus was to get behind Andrew's software, IQWorks.
