---
title: "References"
date: 2017-09-23T22:25:31+01:00
draft: false
menu:
  main:
    parent: About
    identifier: References
    weight: 30
---

## ASTRO 2007

### Automated Analysis Software for the Objective Assessment and Optimisation of Radiotherapy Image Quality

- A.J. Reilly, D.I. Thwaites
- International Journal of Radiation Oncology * Biology * Physics
- 1 November 2007 (Vol. 69, Issue 3, Page S719)
- [Full abstract in the Red Journal](http://www.redjournal.org/article/PIIS0360301607033901/fulltext)
- [Full abstract pdf](/about/astro2007abstract.pdf)

## RSNA 2007

### IQ Works: An Automated Image Analysis Framework for the Objective Assessment and Optimisation of Image Quality

- CODE: SSQ16-06 SESSION: Physics (Various Modalities: Nuclear Medicine, Optical, MR Imaging)
- Thursday, 29th November 2007, 11.20 AM
- [Full abstract on RSNA Website](http://archive.rsna.org/2007/5009358.html)

### A New Package and the Evaluation of Detectors for Display and Projector Luminance Response Curve Calibration against the DICOM Part 14 Grayscale Standard Display Function (GSDF)

- CODE: SSJ16-05 SESSION: Physics (Visualization, Displays, PACS)
- Tuesday, 27th November 2007, 3.40 PM
- [Full abstract on the RSNA Website](http://archive.rsna.org/2007/5016670.html)
- Discusses the Display QA functionality in IQWorks
