---
title: "Who controls IQWorks?"
date: 2017-09-23T22:38:25+01:00
draft: true
menu:
  main:
    parent: About
    identifier: Ownership
    weight: 20
---

IQWorks is not 'owned' by any one group, but several UK groups have a great deal of interest in it's direction and it's success.

[CT User Group](http://ctug.org.uk), Digital Mammography Working Party, [UK Mammography Physics Group](http://ukmpg.org.uk), [IPEM's](https://ipem.ac.uk) Diagnostic Radiology SIG, Informatics and Computing SIG and Radiotherapy SIG and the BIR Rad Phys committee have all been identified as interested groups. In due course, NMSIG, MRSIG and UNIRSIG might also have an interest as the project progresses.

We would like to think that all of these groups can contribute ideas and influence direction of the project, and one proposal is that the various interested parties should have representation on the proposed UK steering group.
The important thing for the success of the project will be securing the endorsement of IQWorks from the IPEM SIGs and the UGs. This would be by referencing the software in guidance documents and acknowledging it in papers. Part of the steering group's role will be to secure these endorsements, raise awareness of the project, encourage/coordinate contributions and involvement and maintain the overall "quality" of the end product. (Endorsements and maintaining quality go hand in hand.)

Longer term, it would be ideal if, when new guidance for a particular QA test was being launched, a module for IQWorks was developed and recommended at the same time.

In addition, some of the groups are already assisting the project in other ways: CTUG is paying for this website (iqworks.org), and we are hoping that once people have got their hands on the software DRSIG will organise an end user training day.

Beyond the UK interest, Andrew and I are both keen to encourage international involement, from both developers and end users. Hopefully Andrew's trip to the states will stimulate this. We're also quite keen that we make it available to countries where medical physics support for diagnostic imaging is sparse or underdeveloped/funded, and a program like this might improve the quality of QA they are able to perform with the resource they have.
